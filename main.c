#include <stdio.h>
#include <string.h>
#define MAX 10

static char caracteres[MAX] = {'a','b','c'};
static long enteros_largos[MAX];
static char *punteros[MAX] = {"hola","mundo","hello","world"};

void iterar_enteros(int *);
void iterar_caracteres(char *);
void iterar_enteros_largos(long *);
void iterar_punteros(char **);

int main()
{
	int enteros[MAX];
	enteros[0] = 100;
	enteros[1] = 300;
	enteros[5] = 50;

	enteros_largos[2] = 1040007;
	enteros_largos[3] = 4000022;
	enteros_largos[4] = 3000340;

	iterar_enteros(enteros);
	iterar_caracteres(caracteres);
	iterar_punteros(punteros);
	iterar_enteros_largos(enteros_largos);
}

void iterar_enteros(int *enteros)
{
	printf("\n\n*--------------Iterar ENTEROS-------------*\n");
	int *p_anterior = NULL;
	int *p;
	for(int i=0;i<MAX;i++)
	{
		p = enteros + i;
		printf("\nEl valor de p es: %d\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);

		p_anterior = p;
	}
}

void iterar_caracteres(char *caracteres){
	printf("\n\n*----------Iterar Caracteres----------------*\n");
	char *p_anterior = NULL;
	char *p;
	for (int i = 0; i < MAX; i++)
	{
		p = caracteres + i;
		printf("\nEl valor de p es: %c\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);
		p_anterior = p;
	}
}

void iterar_enteros_largos(long *enteros_largos){

	printf("\n\n *----------Iterar Punteros Largos----------------*\n");

	long *p_anterior =NULL;

	long *p;
	
	for(int i = 0; i < MAX ; i++)
	{
		p = enteros_largos + i;
		printf("El valor de p es: %ld\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);
		p_anterior = p;
	}

}


void iterar_punteros(char **punteros){
	printf("\n\n*----------Iterar punteros-------------*\n");
	char **p_anterior = NULL;
	char **p;
	for (int i = 0; i < MAX; ++i)
	{
		p = punteros + i;
		printf("\nEl valor de p es: %s\n", *p);
		printf("La dirección de p es: %p\n", p);
		printf("El offset entre direcciones es: %ld\n",(long)p - (long)p_anterior);
		p_anterior = p;
	}
}
